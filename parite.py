#! /usr/bin/env python3
# coding: utf-8
import argparse

import analysis.csv as c_an
import analysis.xml as x_an

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--extension", help=""" Type of your analysis data "xml" or "csv" """) # -r pour la commande --extention pour le parametre
    parser.add_argument("-d", "--datafile", help=""" type du file data name with extention """ )
    return parser.parse_args()
def main():
    args = parse_arguments()    
    datafile = args.datafile
    # import pdb; pdb.set_trace()
    if args.extension == "csv":
        c_an.launch_analysis("current.csv")#"current_mps.csv"
    elif args.extension == "xml":
        x_an.launch_analysis(datafile)#"SyceronBrut.xml"


if __name__ == "__main__":
    main()
