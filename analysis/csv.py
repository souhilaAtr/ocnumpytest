import os
import logging as lg
lg.basicConfig(level=lg.DEBUG)
def launch_analysis(data_file):
    directory= os.path.dirname( os.path.dirname(__file__)) #renvoi le dossier du fichier passer en argument __file__=csv alors le dossier analyis
    print(f" le chemin u dossier parent {directory}")#on lui dis de renvoyer le dossier du dossier
    path_to_file = os.path.join(directory, "data", data_file) #join premet de reconstruir le chemin  
    print(f" le chemin du fichier {path_to_file}") 
    try:
        with open(path_to_file, "r") as myfile:
            preview = myfile.readline()
        lg.debug("aficher le preview {}".format(preview))
    except FileNotFoundError as e:
        lg.critical("le fichier est introuvable: {}".format(e) )
        
       
    
def main():
    launch_analysis("current_mps.csv")
        


if __name__ == "__main__":
    main()